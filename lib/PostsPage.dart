import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Post {
    final String title;
    final String content;
    final String? imagePath;
    List<String> comments;

    Post(this.title, this.content, {this.imagePath, this.comments = const []});
}

class PostsPage extends StatefulWidget {
  const PostsPage({Key? key}) : super(key: key);

  @override
  _PostsPageState createState() => _PostsPageState();
}

class _PostsPageState extends State<PostsPage> {
  List<Post> posts = [];

  @override
  void initState() {
    super.initState();
    _getPrefs();
  }

  Future<void> _getPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      posts = _decodePosts(prefs.getStringList('posts') ?? []);
    });
  }

  void _deletePost(int index) {
    setState(() {
      posts.removeAt(index);
    });
    _updatePref();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Posts', style: TextStyle(color: Colors.purple, fontWeight: FontWeight.bold)),
      ),
      body: ListView.builder(
        itemCount: posts.length,
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => EditPostPage(
                    post: posts[index],
                    onSave: (updatedPost) {
                      _editPost(updatedPost, index);
                    },
                  ),
                ),
              );
            },
            child: Card(
              margin: const EdgeInsets.all(8.0),
              child: ListTile(
                title: Text(posts[index].title),
                subtitle: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(_getExcerpt(posts[index].content)),
                    TextButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => CommentsPage(post: posts[index]),
                          ),
                        );
                      },
                      child: const Text('Commentaires'),
                    ),
                  ],
                ),
                leading: posts[index].imagePath != null ? Image.file(File(posts[index].imagePath!)) : null,
                trailing: IconButton(
                  icon: const Icon(Icons.delete),
                  onPressed: () => _deletePost(index),
                ),
              ),
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => _addNewPost('Nouveau Titre', 'Nouveau contenu. Lorem ipsum dolor sit amet.'),
        child: const Icon(Icons.add),
      ),
    );
  }

  void _addNewPost(String title, String content) {
    if (title.isEmpty || content.isEmpty) {
      // Ne pas ajouter de post si le titre ou le contenu sont vides.
      return;
    }

    setState(() {
      posts.add(Post(title, content));
    });
    _updatePref();
  }

  void _editPost(Post updatedPost, int index) {
    setState(() {
      posts[index] = updatedPost;
    });
    _updatePref();
  }

  Future<void> _updatePref() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setStringList('posts', _encodePosts(posts));
  }

  String _getExcerpt(String content) {
    return content.substring(0, content.length >= 50 ? 50 : content.length) + '...';
  }

  List<String> _encodePosts(List<Post> posts) {
    return posts.map((post) {
      final commentsString = post.comments.join('|');
      return '${post.title}|${post.content}|${post.imagePath ?? ""}|${commentsString}';
    }).toList();
  }

  List<Post> _decodePosts(List<String> encodedPosts) {
    return encodedPosts.map((encodedPost) {
      List<String> parts = encodedPost.split('|');
      final comments = parts.sublist(3);
      return Post(parts[0], parts[1], imagePath: parts[2].isNotEmpty ? parts[2] : null, comments: comments);
    }).toList();
  }
}

class EditPostPage extends StatefulWidget {
  final Post post;
  final Function(Post) onSave;
  const EditPostPage({Key? key, required this.post, required this.onSave}) : super(key: key);

  @override
  _EditPostPageState createState() => _EditPostPageState();
}

class _EditPostPageState extends State<EditPostPage> {
  late TextEditingController _titleController;
  late TextEditingController _contentController;
  String? _imagePath;

  @override
  void initState() {
    super.initState();
    _titleController = TextEditingController(text: widget.post.title);
    _contentController = TextEditingController(text: widget.post.content);
    _imagePath = widget.post.imagePath;
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Modifier le post', style: TextStyle(color: Colors.purple, fontWeight: FontWeight.bold)),
        actions: [
          IconButton(
            icon: const Icon(Icons.save),
            onPressed: _savePost,
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextField(
              controller: _titleController,
              decoration: const InputDecoration(labelText: 'Titre'),
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    TextField(
                      controller: _contentController,
                      decoration: const InputDecoration(labelText: 'Contenu'),
                      keyboardType: TextInputType.multiline,
                      maxLines: null,
                    ),
                    const SizedBox(height: 20),
                    if (_imagePath != null)
                        Image.file(File(_imagePath!)),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _pickImage,
        child: const Icon(Icons.image),
      ),
    );
  }

  void _pickImage() async {
    final picker = ImagePicker();
    final pickedImage = await picker.pickImage(source: ImageSource.gallery);
    if (pickedImage != null) {
        setState(() {
          _imagePath = pickedImage.path;
        });
    }
  }

  void _savePost() {
    Post updatedPost = Post(_titleController.text, _contentController.text, imagePath: _imagePath);
    widget.onSave(updatedPost);
    Navigator.of(context).pop();
  }

  @override
  void dispose() {
    _titleController.dispose();
    _contentController.dispose();
    super.dispose();
  }
}

class CommentsPage extends StatefulWidget {
  final Post post;
  const CommentsPage({Key? key, required this.post}) : super(key: key);

  @override
  _CommentsPageState createState() => _CommentsPageState();
}

class _CommentsPageState extends State<CommentsPage> {
    late TextEditingController _commentController;

    @override
    void initState() {
        super.initState();
        _commentController = TextEditingController();
    }
Future<void> _updatePref() async {
    // Code pour mettre à jour les préférences partagées
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> encodedPosts = prefs.getStringList('posts') ?? [];
    
    // Recherchez l'indice du post actuel dans la liste des posts encodés
    int postIndex = encodedPosts.indexWhere((encodedPost) {
        return encodedPost.contains(widget.post.title) &&
               encodedPost.contains(widget.post.content);
    });
    
    // Mettez à jour le post encodé avec les commentaires actuels
    if (postIndex != -1) {
        encodedPosts[postIndex] = '${widget.post.title}|${widget.post.content}|${widget.post.imagePath ?? ""}|${widget.post.comments.join("|")}';
        await prefs.setStringList('posts', encodedPosts);
    }
}



    @override
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: AppBar(
                title: const Text('Commentaires'),
            ),
            body: Column(
                children: [
                    Expanded(
                        child: ListView.builder(
                            itemCount: widget.post.comments.length,
                            itemBuilder: (context, index) {
                                return ListTile(
                                    title: Text(widget.post.comments[index]),
                                );
                            },
                        ),
                    ),
                    _buildCommentInput(),
                ],
            ),
        );
    }

    Widget _buildCommentInput() {
        return Padding(
            padding: const EdgeInsets.all(16.0),
            child: Row(
                children: [
                    Expanded(
                        child: Container(
                            decoration: BoxDecoration(
                                color: Colors.grey[200],
                                borderRadius: BorderRadius.circular(12.0),
                            ),
                            padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                            child: TextField(
                                controller: _commentController,
                                decoration: InputDecoration(
                                    hintText: 'Ajouter un commentaire',
                                    border: InputBorder.none,
                                    prefixIcon: Icon(Icons.comment, color: Colors.grey),
                                ),
                            ),
                        ),
                    ),
                    const SizedBox(width: 8),
                    IconButton(
                        icon: const Icon(Icons.send, color: Colors.purple),
                        onPressed: _addComment,
                    ),
                ],
            ),
        );
    }

void _addComment() async {
    final comment = _commentController.text.trim();
    if (comment.isNotEmpty) {
        setState(() {
            widget.post.comments.add(comment);
            _commentController.clear();
        });

        try {
            await _updatePref();
        } catch (e) {
            // Afficher une erreur à l'utilisateur en cas de problème
            ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                    content: Text('Erreur lors de la mise à jour des préférences : $e'),
                    backgroundColor: Colors.red,
                ),
            );
        }
    }
}


    Future<void> _updatePreferences() async {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        List<String> encodedPosts = prefs.getStringList('posts') ?? [];
        
        for (int i = 0; i < encodedPosts.length; i++) {
            final encodedPost = encodedPosts[i];
            final parts = encodedPost.split('|');
            
            // Vérifiez que le post encodé correspond au post actuel.
            if (parts[0] == widget.post.title && parts[1] == widget.post.content && parts[2] == widget.post.imagePath) {
                parts[3] = widget.post.comments.join('|');
                encodedPosts[i] = parts.join('|');
                break;
            }
        }

        await prefs.setStringList('posts', encodedPosts);
    }

    @override
    void dispose() {
        _commentController.dispose();
        super.dispose();
    }
}


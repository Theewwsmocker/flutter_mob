import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'home_page.dart';
import 'login_page.dart';
import 'signup_page.dart';
import 'profile_page.dart'; // Importer la page de profil
import 'PostsPage.dart'; // Importer la page de discussions
import 'messages_page.dart'; // Importer la page de messages

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<bool>(
      future: checkLoginStatus(), // Vérifiez l'état de connexion au démarrage de l'application
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return CircularProgressIndicator(); // Affichez un indicateur de chargement en attendant la vérification de l'état de connexion
        } else {
          final isLoggedIn = snapshot.data ?? false; // Si la vérification échoue, définissez isLoggedIn sur false

          return MaterialApp(
            debugShowCheckedModeBanner:false,
            title: 'Flutter Demo Test',
            theme: ThemeData(
              colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
              useMaterial3: true,
            ),
            initialRoute: isLoggedIn ? '/' : '/login', // Redirigez vers la page d'accueil ou de connexion en fonction de l'état de connexion
            routes: {
              '/': (context) => HomePage(),
              '/profile': (context) => ProfilePage(),
              '/posts': (context) => PostsPage(),
              '/messages': (context) => MessagesPage(),
              '/login': (context) => LoginPage(),
              '/signup': (context) => SignUpPage(),
            },
          );
        }
      },
    );
  }

  Future<bool> checkLoginStatus() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool('isLoggedIn') ?? false; // Renvoie true si l'utilisateur est connecté, sinon false
  }
}

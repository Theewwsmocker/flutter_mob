import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

// Classe représentant un message
class Message {
  final String sender;
  final String content;
  final DateTime time;

  Message(this.sender, this.content, this.time);
}

class MessagesPage extends StatefulWidget {
  const MessagesPage({Key? key}) : super(key: key);

  @override
  _MessagesPageState createState() => _MessagesPageState();
}

class _MessagesPageState extends State<MessagesPage> {
  List<Message> messages = [];
  final TextEditingController _textController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _loadMessages();
  }

  Future<void> _loadMessages() async {
    final prefs = await SharedPreferences.getInstance();
    final List<String>? messageList = prefs.getStringList('messages');
    if (messageList != null) {
      setState(() {
        messages = messageList.map((msg) {
          final parts = msg.split('|');
          return Message(parts[0], parts[1], DateTime.parse(parts[2]));
        }).toList();
      });
    }
  }

  Future<void> _saveMessages() async {
    final prefs = await SharedPreferences.getInstance();
    final List<String> messageList = messages.map((msg) => '${msg.sender}|${msg.content}|${msg.time.toIso8601String()}').toList();
    await prefs.setStringList('messages', messageList);
  }

  void _sendMessage() {
    final String messageText = _textController.text.trim();
    if (messageText.isNotEmpty) {
      setState(() {
        messages.add(Message('Moi', messageText, DateTime.now()));
        _textController.clear();
      });
      _saveMessages();
    }
  }

  void _editMessage(String updatedContent, int index) {
    if (updatedContent.isNotEmpty) {
      setState(() {
        messages[index] = Message(messages[index].sender, updatedContent, DateTime.now());
      });
      _saveMessages();
    }
  }

  void _deleteMessage(int index) {
    setState(() {
      messages.removeAt(index);
    });
    _saveMessages();
  }

  void _showEditDialog(int index) {
    TextEditingController editController = TextEditingController(text: messages[index].content);
    showDialog(
        context: context,
        builder: (context) {
            return AlertDialog(
                title: const Text("Modifier le message"),
                content: TextField(controller: editController),
                actions: [
                    TextButton(
                        onPressed: () {
                            Navigator.of(context).pop();
                            _editMessage(editController.text, index);
                        },
                        child: const Text("Save"),
                    ),
                ],
            );
        },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Messages', style: TextStyle(color: Colors.purple, fontWeight: FontWeight.bold)),
      ),
      body: Column(
        children: [
          Expanded(
            child: ListView.builder(
              itemCount: messages.length,
              itemBuilder: (context, index) {
                return ListTile(
                  title: Text(messages[index].sender, style: TextStyle(fontWeight: FontWeight.bold)),
                  subtitle: Text(messages[index].content),
                  trailing: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      IconButton(
                        icon: const Icon(Icons.edit),
                        onPressed: () => _showEditDialog(index),
                      ),
                      IconButton(
                        icon: const Icon(Icons.delete),
                        onPressed: () => _deleteMessage(index),
                      ),
                    ],
                  ),
                );
              },
            ),
          ),
          Divider(height: 1),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              controller: _textController,
              decoration: InputDecoration(
                labelText: 'Envoyer un message...',
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(30.0),
                  borderSide: BorderSide.none,
                ),
                filled: true,
                fillColor: Colors.grey[200],
                suffixIcon: IconButton(
                  icon: Icon(Icons.send, color: Theme.of(context).primaryColor),
                  onPressed: _sendMessage,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    _textController.dispose();
    super.dispose();
  }
}
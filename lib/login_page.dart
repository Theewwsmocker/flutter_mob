import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final TextEditingController emailController = TextEditingController();
    final TextEditingController passwordController = TextEditingController();

    void login() async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      final String? storedEmail = prefs.getString('email');
      final String? storedPassword = prefs.getString('password');
      final String email = emailController.text;
      final String password = passwordController.text;

      if (email == storedEmail && password == storedPassword) {
        // Authentification réussie, redirigez vers la page d'accueil
        Navigator.pushReplacementNamed(context, '/');
      } else {
        // Afficher un message d'erreur en cas d'informations incorrectes
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text('Adresse e-mail ou mot de passe incorrect'),
          ),
        );
      }
    }

    void navigateToSignUp() {
      Navigator.pushNamed(context, '/signup');
    }

    return Scaffold(
      appBar: AppBar(

      ),
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Connexion", style: TextStyle(fontSize: 29, fontWeight: FontWeight.bold, color: Colors.purple)), // Couleur du titre modifiée ici
              SizedBox(height: 50), // Espace entre le titre et le premier input
              SizedBox(
                width: 300, // Contrôle la largeur des inputs
                child: TextField(
                  controller: emailController,
                  decoration: InputDecoration(
                    labelText: 'Adresse e-mail',
                    border: OutlineInputBorder(), // Bordure arrondie
                    contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                  ),
                  textAlign: TextAlign.center, // Texte centré
                ),
              ),
              SizedBox(height: 20),
              SizedBox(
                width: 300, // Contrôle la largeur des inputs
                child: TextField(
                  controller: passwordController,
                  decoration: InputDecoration(
                    labelText: 'Mot de passe',
                    border: OutlineInputBorder(), // Bordure arrondie
                    contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                  ),
                  obscureText: true,
                  textAlign: TextAlign.center, // Texte centré
                ),
              ),
              SizedBox(height: 20),
              ElevatedButton(
                onPressed: login,
                child: Text('Se connecter'),
              ),
              TextButton(
                onPressed: navigateToSignUp,
                child: Text("Pas encore inscrit ? S'inscrire"),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

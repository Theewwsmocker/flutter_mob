import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Accueil', style: TextStyle(color: Colors.purple, fontWeight: FontWeight.bold)),
        leading: IconButton(
          icon: const Icon(Icons.menu),
          onPressed: () {
            _scaffoldKey.currentState?.openDrawer();
          },
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
            child: Text(
              'Bienvenue sur notre application de discussion ! Connectez-vous avec d\'autres utilisateurs et partagez vos idées.',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 16.0,
              ),
            ),
          ),
        ],
      ),
      drawer: Drawer(
        child: FutureBuilder<bool>(
          future: isLoggedIn(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return CircularProgressIndicator();
            } else {
              return buildDrawerItems(context, snapshot.hasData && snapshot.data!);
            }
          },
        ),
      ),
    );
  }

  Widget buildDrawerItems(BuildContext context, bool isLoggedIn) {
    return ListView(
      padding: EdgeInsets.zero,
      children: <Widget>[
        SizedBox(
          height: 150,
          child: DrawerHeader(
            decoration: BoxDecoration(
              color: Colors.purple,
            ),
            child: Center(
              child: Text(
                'Menu',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 24,
                ),
              ),
            ),
          ),
        ),
        ListTile(
          leading: Icon(Icons.person, color: Colors.blue),
          title: Text('Profil'),
          onTap: () {
            Navigator.pop(context);
            Navigator.pushNamed(context, '/profile');
          },
        ),
        ListTile(
          leading: Icon(Icons.forum, color: Colors.green),
          title: Text('Posts'),
          onTap: () {
            Navigator.pop(context);
            Navigator.pushNamed(context, '/posts');
          },
        ),
        ListTile(
          leading: Icon(Icons.message, color: Colors.purple),
          title: Text('Messages'),
          onTap: () {
            Navigator.pop(context);
            Navigator.pushNamed(context, '/messages');
          },
        ),
        ListTile(
          leading: Icon(Icons.exit_to_app, color: Colors.red),
          title: Text('Déconnexion'),
          onTap: () async {
            // Déconnecter l'utilisateur
            SharedPreferences prefs = await SharedPreferences.getInstance();
            await prefs.setBool('isLoggedIn', false);
            Navigator.pushReplacementNamed(context, '/login');
          },
        ),
      ],
    );
  }

  Future<bool> isLoggedIn() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('email') != null;
  }
}
